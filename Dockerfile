FROM jenkins:2.60.3-alpine

MAINTAINER Leadel Ngalamo <leangalamo2002@yahoo.com>

## get rid of admin password setup
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"

COPY files/plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
